import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus';
import StoragePlugin from 'vue-web-storage';
//import { BootstrapVue } from 'bootstrap-vue' // Incompatible with Vue 3

import 'element-plus/lib/theme-chalk/index.css';
//import 'bootstrap/dist/css/bootstrap.css' // Incompatible with Vue 3
//import 'bootstrap-vue/dist/bootstrap-vue.css' // Incompatible with Vue 3

const app = createApp(App);

app.use(router).use(router);
app.use(ElementPlus);
app.use(StoragePlugin);
app.mount('#app');
